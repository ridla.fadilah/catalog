update product set weight=12.34;

insert into product_photos(id, id_product, url) VALUES
('p001i001', 'p001', '/p001/img001.jpg');
insert into product_photos(id, id_product, url) VALUES
('p001i002', 'p001', '/p001/img002.jpg');
insert into product_photos(id, id_product, url) VALUES
('p001i003', 'p001', '/p001/img003.jpg');

insert into product_photos(id, id_product, url) VALUES
('p002i001', 'p002', '/p002/img001.jpg');
insert into product_photos(id, id_product, url) VALUES
('p002i002', 'p002', '/p002/img002.jpg');
insert into product_photos(id, id_product, url) VALUES
('p002i003', 'p002', '/p002/img003.jpg');

insert into product_photos(id, id_product, url) VALUES
('p003i001', 'p003', '/p003/img001.jpg');
insert into product_photos(id, id_product, url) VALUES
('p003i002', 'p003', '/p003/img002.jpg');
insert into product_photos(id, id_product, url) VALUES
('p003i003', 'p003', '/p003/img003.jpg');

insert into product_photos(id, id_product, url) VALUES
('p004i001', 'p004', '/p004/img001.jpg');
insert into product_photos(id, id_product, url) VALUES
('p004i002', 'p004', '/p004/img002.jpg');
insert into product_photos(id, id_product, url) VALUES
('p004i003', 'p004', '/p004/img003.jpg');

insert into product_photos(id, id_product, url) VALUES
('p005i001', 'p005', '/p005/img001.jpg');
insert into product_photos(id, id_product, url) VALUES
('p005i002', 'p005', '/p005/img002.jpg');
insert into product_photos(id, id_product, url) VALUES
('p005i003', 'p005', '/p005/img003.jpg');

ALTER TABLE product
  ADD COLUMN price DECIMAL (19,2);

UPDATE product set price=100000.00;

ALTER TABLE product
  ALTER COLUMN price set NOT NULL ;

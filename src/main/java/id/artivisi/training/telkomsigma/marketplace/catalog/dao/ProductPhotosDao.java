package id.artivisi.training.telkomsigma.marketplace.catalog.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.artivisi.training.telkomsigma.marketplace.catalog.entity.Product;
import id.artivisi.training.telkomsigma.marketplace.catalog.entity.ProductPhotos;

/**
 * @author <a href="mailto:ridla.fadilah@gmail.com">Ridla Fadilah</a>
 */
public interface ProductPhotosDao extends PagingAndSortingRepository<ProductPhotos, String> {
    public Iterable<ProductPhotos> findByProduct(Product product);
}

package id.artivisi.training.telkomsigma.marketplace.catalog.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity @Data
public class Product {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty @Size(max = 100)
    private String code;

    @NotNull @NotEmpty
    private String name;

    @NotNull @Min(1)
    private BigDecimal weight;

    @NotNull @Min(1000)
    private BigDecimal price;

}
